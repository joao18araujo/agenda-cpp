#ifndef CONTATO_H
#define CONTATO_H

#include "pessoa.hpp"

class Contato : public Pessoa{
	private:
		string email;
		string fax;

	public:
		Contato();
		Contato(string email, string fax);
		string getEmail();
		void setEmail(string email);
		string getFax();
		void setFax(string fax);
};

#endif
