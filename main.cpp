#include <iostream>
#include "amigo.hpp"
#include "contato.hpp"

using namespace std;

int main () {
	
	Pessoa umaPessoa;
	Amigo umAmigo;
	Contato umContato;

	umaPessoa.setNome("Joao Vitor");
	umaPessoa.setIdade("18");
	umaPessoa.setTelefone("3284-4732");
	
	umAmigo.setNome("Glauber");
	umAmigo.setIdade("18");
	umAmigo.setTelefone("8234-3252");
	umAmigo.setAniversario("11/06");
	umAmigo.setApelido("Carinha da banda");

	umContato.setNome("Carlos");
	umContato.setIdade("37");
	umContato.setTelefone("3352-2342");
	umContato.setEmail("carlos_moreira@gmail.com");
	umContato.setFax("3345-2398");

	cout << "Nome da pessoa: " << umaPessoa.getNome() << endl;
	cout << "Idade da pessoa: " << umaPessoa.getIdade() << endl;
	cout << "Telefone da pessoa: " << umaPessoa.getTelefone() << endl << endl;

	cout << "Nome do amigo: " << umAmigo.getNome() << endl;
	cout << "Idade do amigo: " << umAmigo.getIdade() << endl;
	cout << "Telefone do amigo: " << umAmigo.getTelefone() << endl;
	cout << "Aniversario do amigo: " << umAmigo.getAniversario() << endl;
	cout << "Apelido do amigo: " << umAmigo.getApelido() << endl << endl;
	
	cout << "Nome do contato: " << umContato.getNome() << endl;
	cout << "Idade do contato: " << umContato.getIdade() << endl;
	cout << "Telefone do contato: " << umContato.getTelefone() << endl;
	cout << "Email do contato: " << umContato.getEmail() << endl;
	cout << "Fax do contato: " << umContato.getFax() << endl;

	
	return 0;
}
