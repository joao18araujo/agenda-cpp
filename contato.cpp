#include "contato.hpp"

Contato::Contato(){
	setEmail("_______@____.com");
	setFax("____-____");
}

Contato::Contato(string email, string fax){
	this->email = email;
	this->fax = fax;
}

string Contato::getEmail(){
	return email;
}

void Contato::setEmail(string email){
	this->email = email;
}

string Contato::getFax(){
	return fax;
}

void Contato::setFax(string fax){
	this->fax = fax;
}
