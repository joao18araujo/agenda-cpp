#include "amigo.hpp"

Amigo::Amigo(){
	setAniversario("__/__");
	setApelido("________");
}

Amigo::Amigo(string aniversario, string apelido){
	this->aniversario = aniversario;
	this->apelido = apelido;
}

string Amigo::getAniversario(){
	return aniversario;
}

void Amigo::setAniversario(string aniversario){
	this->aniversario = aniversario;
}

string Amigo::getApelido(){
	return apelido;
}

void Amigo::setApelido(string apelido){
	this->apelido = apelido;
}
