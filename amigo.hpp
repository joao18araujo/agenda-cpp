#ifndef AMIGO_H
#define AMIGO_H

#include "pessoa.hpp"

class Amigo : public Pessoa{
	private:
		string aniversario;
		string apelido;

	public:
		Amigo();
		Amigo(string aniversario, string apelido);
		string getAniversario();
		void setAniversario(string aniversario);
		string getApelido();
		void setApelido(string apelido);
};

#endif
